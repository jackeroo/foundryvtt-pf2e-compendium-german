<p data-sourcepos="1:1-1:20" dir="auto"><strong>WORK IN PROGRESS</strong></p>

<p data-sourcepos="3:1-3:116" dir="auto">Das Modul bietet eine einfache M&ouml;glichkeit die Inhalte der PF2 Kompendien (Compendium) ins Deutsche zu &uuml;bersetzen.</p>

<p data-sourcepos="5:1-8:40" dir="auto"><strong>1. Voraussetzungen</strong> Um das Modul nutzen zu k&ouml;nnen zwingend das Babele Modul von Simone installiert werden: Manifest: <a href="https://gitlab.com/riccisi/foundryvtt-babele/raw/master/module/module.json">https://gitlab.com/riccisi/foundryvtt-babele/raw/master/module/module.json</a> oder im Addon-Menu nach &quot;Babele&quot; suchen.</p>

<p data-sourcepos="10:1-12:84" dir="auto"><strong>2. Installation</strong> Die folgende Manifest URL kopieren und im Addon-Menu einf&uuml;gen <a href="https://gitlab.com/jackeroo/foundryvtt-pf2e-compendium-german/raw/master/module.json">https://gitlab.com/jackeroo/foundryvtt-pf2e-compendium-german/raw/master/module.json</a></p>

<p data-sourcepos="14:1-16:42" dir="auto"><strong>3. Module aktivieren</strong> Im Spiel die Module (Babele und PF2e Compendium German) noch aktivieren und fertig! Weitere Einstellungen sind nicht notwendig</p>

<p dir="auto">Aktueller Stand der &Uuml;bersetzungen (V0.25):</p>

<table border="1" cellpadding="1" cellspacing="1" dir="auto">
	<thead>
		<tr>
			<th scope="col">Status</th>
			<th scope="col">Datei</th>
			<th scope="col">User</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><span style="background-color:green">DONE</span></td>
			<td>pf2e.conditionspf2e</td>
			<td>eregark</td>
		</tr>
		<tr>
			<td><span style="background-color:#ffff00">DONE</span></td>
			<td>pf2e.actionspf2e</td>
			<td>eregark</td>
		</tr>
		<tr>
			<td><span style="background-color:#00ff00">DONE</span> (Shoony are still missing)</td>
			<td>pf2e.ancestryfeatures</td>
			<td>eregark</td>
		</tr>
		<tr>
			<td><span style="background-color:#ffff00">WORK</span></td>
			<td>pf2e.equipment-srd</td>
			<td>jackeroo</td>
		</tr>
		<tr>
			<td>OPEN</td>
			<td>pf2e.classfeatures</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>OPEN</td>
			<td>pf2e.feats-srd</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>OPEN</td>
			<td>pf2e.spells-srd</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>...</td>
			<td>...</td>
			<td>...</td>
		</tr>
	</tbody>
</table>

<p dir="auto">&nbsp;</p>
