Hooks.once('init', () => {

	if(typeof Babele !== 'undefined') {
		
		Babele.get().register({
			module: 'pf2e-compendium-german',
			lang: 'de',
			dir: 'compendium'
		});
	}
});